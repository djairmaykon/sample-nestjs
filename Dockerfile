# Primeira imagem para build da aplicação e rodar lint e testes
# Pega imagem mais recente do node
FROM node:18-alpine3.14 AS development

# define diretorio de trabalho como /usr/src/app
WORKDIR /usr/src/app

# copia o package json e o package-lock para o container
COPY package*.json ./

# instala as dependencias
RUN npm install

# copia todos os arquivos da aplicação para o container
COPY . .

# roda lint testes e então faz o build da aplicação
RUN npm run lint && npm run test && npm run test:e2e && npm run build

# quando o container subir inicia a aplicação 
CMD ["node", "dist/main"]